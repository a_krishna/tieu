name := "risk-tieu"

organization in ThisBuild := "com.rover"


scalaVersion in ThisBuild := "2.11.7"

ivyScala := ivyScala.value map { _.copy(overrideScalaVersion = true) }

scalacOptions += "-Ylog-classpath"

// https://mvnrepository.com/artifact/org.scalatest/scalatest
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.6-SNAP5" % Test


