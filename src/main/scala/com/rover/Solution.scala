package com.rover

import java.io.{BufferedReader, InputStream, InputStreamReader}

import scala.collection.mutable


object OrderProcessor extends App {
  //CW=ACT + (PAT-CAT)+PW
  def bookingDiff(t2: (Long, Long)) = -t2._1

  def cookingDiff(t2: (Int, Int)) = -t2._2


  val tasks = new mutable.PriorityQueue[(Long, Long)]()(Ordering.by(bookingDiff))


  def processQData(data: mutable.PriorityQueue[(Long, Long)], n: Int): Long = {

    val currentOrder = data.dequeue()
    val rev = data.toQueue.sortBy(_._2)
    val res = rev.foldLeft(currentOrder._2, currentOrder._1 + currentOrder._2) { (prev, laterOrder) =>
      val currentTime = prev._2 + laterOrder._2
      val waitingTime = currentTime - laterOrder._1
      (prev._1 + waitingTime, currentTime)
    }._1 / n
    res
  }


  def process(in: InputStream): Either[String, Long] = {
    try {
      val br = new BufferedReader(new InputStreamReader(in))
      val n = br.readLine().toInt
      val customers = (0 to n - 1).toList.map { z =>
        val x = br.readLine().split(" ").map(_.trim.toLong)
        tasks.enqueue((x.head, x.last))
      }
      br.close()
      Right(processQData(tasks, n))
    } catch {
      case exe: Exception => Left(exe.getMessage)
    }
  }


  process(System.in)


}
