package com.rover

object TestMain extends App {
  def maxWords(a:Seq[String],b:Set[String]):String={

    b.foldLeft((0,"")){(z,c) =>
      val x=a.filter(_==c)
      if(z._1 < x.size){
        (x.size,x.head)
      }else z
    }._2
  }

  val s = "cat dog cow cat"

  val c=s.split(" ")
  print(maxWords(c,c.toSet))
}
