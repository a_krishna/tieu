package com.rover

import org.scalatest.FlatSpec

import scala.collection.mutable

class TestOrderProcessor extends FlatSpec{

  "OrderProcessor" should "match minimum waiting time 9" in {
    import OrderProcessor._
    val tasks=new mutable.PriorityQueue[(Long,Long)]()(Ordering.by(bookingDiff))
    tasks.enqueue((0,3))
    tasks.enqueue((1,9))
    tasks.enqueue((2,6))
    assert(processQData(tasks,3) == 9L)
  }

  "OrderProcessor" should "return minimum waiting time 8" in {
    import OrderProcessor._

    tasks.enqueue((0,3))
    tasks.enqueue((1,9))
    tasks.enqueue((2,5))
    assert(processQData(tasks,3) == 8L)
  }


  "OrderProcessor" should "return minimum waiting time 6" in {
    import OrderProcessor._

    tasks.enqueue((0,9))
    tasks.enqueue((10,4))
    assert(processQData(tasks,2) == 6L)
  }
}
